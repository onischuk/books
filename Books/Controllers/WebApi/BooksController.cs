﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Books.Controllers.WebApi
{
    public class BooksController : ApiController
    {
        [HttpGet]
        [ActionName("")]
        public IHttpActionResult Index()
        {
            return Ok(new { data = "test data, ept" });
        }
    }
}
