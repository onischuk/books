﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Books.Data.Entities
{
    public class Author
    {
        public int Id { get; set; }
        public string FirstName { get;set; }
        public string LastName { get; set; }
        public int BooksCount { get; set; }        

        public virtual ICollection<Book> Books { get; set; }
    }
}