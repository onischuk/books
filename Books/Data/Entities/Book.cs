﻿using System;
using System.Collections.Generic;

namespace Books.Data.Entities
{
    public class Book
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public DateTime PublicationDate { get; set; }
        public int Raiting { get; set; }
        public int NumberOfPages { get; set; }
        public byte[] Image { get; set; }

        public virtual ICollection <Author> Authors { get; set; }
    }
}