﻿using Books.Data.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace Books.Data
{
    public class BooksContext: DbContext
    {

        public DbSet<Book> Books { get; set; }

        public DbSet<Author> Authors { get; set; } 

        public BooksContext():base("DefaultConnection")
        {

        }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Entity<Book>().HasRequired(b => b.Title);
            modelBuilder.Entity<Book>().HasRequired(b => b.Authors);
            modelBuilder.Entity<Book>()
                .HasMany(b => b.Authors)
                .WithMany(a => a.Books);

            modelBuilder.Entity<Author>().HasRequired(a => a.FirstName);
            modelBuilder.Entity<Author>().HasRequired(a => a.LastName);
        }
    }
}