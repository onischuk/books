﻿$(document).ready(function () {
    $('#example').DataTable({
        "bSort": false,
        searching: false, 
        "createdRow": function (row, data, index) {
            $(row).append("<td><button class='ui red compact icon button'> <i class='delete icon'></i></button><button class='ui blue compact icon button'> <i class='edit icon'></i></button></td>");          
        },
        "initComplete": function (settings, json) {
            $("#example thead tr").append("<th></th>");
        },        
    });
});