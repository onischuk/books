﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Books.Data;
using Books.Data.Entities;

namespace Books.Repositories
{
    public class BooksRepository : IBooksRepository
    {
        protected readonly BooksContext context;

        public BooksRepository(BooksContext context) => this.context = context;


        public IEnumerable<Book> GetAll()
            => context.Books.ToList();
    }
}