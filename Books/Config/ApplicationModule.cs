﻿using Autofac;
using Books.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Books.Config
{
    public class ApplicationModule: Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<BooksContext>()
                .InstancePerRequest();

            builder.RegisterAssemblyTypes(GetType().Assembly)
                .AsImplementedInterfaces()
                .InstancePerRequest();
        }
    }
}